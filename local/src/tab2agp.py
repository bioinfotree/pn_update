#!/bin/env python
# Copyright Davide Scaglione 2016 <dscaglione@igatechnology.com>

import argparse
import sys
import ipdb
from os import path as path_module


class Element(object):
    def __init__(self, name, orientation):
        self.name = name
        if orientation in ['+','F','0']:     
            self._reverse = False
        elif orientation in ['-','R','1']:
            self._reverse = True
        else:
            sys.exit("Unable to get orientation for %s" % name)
        #print self._reverse

    @property
    def rc(self):
        return self._reverse


class Group(object):
    def __init__(self, name):
        self.name = name
        self._elements = []

    @property
    def elements(self):
        return self._elements
    
    def attach_elem(self, element):
        self._elements.append(element)


def read_group(myfile):
    with open(myfile) as f:
        for line in f.readlines():
            if line.startswith("#"):
                continue
            fields = line.strip().split("\t")
            yield fields


def unknown_gap(scaff_name, start, counter, **kwargs):
    size = kwargs.get('size', 1000)
    parts = [scaff_name, start , start + size - 1, counter, 'N', size, 'contig', 'no', 'na']
    parts = map(str, parts)
    return "\t".join(parts)

def full_contig(scaff_name, start, end, counter, contig_name, c_start, c_end, orientation):
    parts = [scaff_name, start , end , counter, 'W', contig_name, c_start, c_end, orientation]
    parts = map(str, parts)
    return "\t".join(parts)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Genrate AGP from grouping files',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--paths', dest='paths',
                        required=True,nargs='*',
                        help='A list of files containing ordering information')
    parser.add_argument('--all-contigs-size', dest='sizes',
                        required=True,
                        help='A two-column file with contig name and size')
    parser.add_argument('--element-column', dest='ecolumn',
                        required=True, type=int,
                        help='Column containing element name (contigs)')
    parser.add_argument('--orientation-column', dest='ocolumn',
                        required=True, type=int,
                        help='Column containing orientation information (+|F|0 or -|R|1)')
    parser.add_argument('--score-column', dest='scolumn',
                        required=False, type=int, default=None,
                        help='Column containing scoring  (float) to be used for splitting')
    parser.add_argument('--score-threshold', dest='minscore',
                        required=False, default=None,
                        help='If a score column is provided this limit will trigger splitting')
    

    my_args = parser.parse_args()

    #prefix = 'scaffold_'
    counter = 0
    groups = []

    sizes = {}
    with open(my_args.sizes) as s:
        for line in s.readlines():
            [name, size] = line.strip().split("\t")
            sizes[name] = int(size)

    for path in my_args.paths:

        prefix = path_module.splitext(path_module.basename(path))[0] # modified here

        counter += 1

        groups.append(Group(prefix))
        for fields in read_group(path):
            #ipdb.set_trace() 
            #print groups[-1]
            if my_args.scolumn and float(fields[my_args.scolumn - 1]) < float(my_args.minscore):
                if len(groups[-1].elements) > 0:
                    counter += 1
                    groups.append(Group(prefix))
                continue
            name = fields[my_args.ecolumn - 1]
            orientation = fields[my_args.ocolumn - 1]
            groups[-1].attach_elem(Element(name, orientation))

    counter = 1
    used_elements = []
    for group in groups:
        start = 1
        for element in group.elements:
            if element.name in used_elements:
                sys.exit("Element is invoked twice %s" % element.name)
            else:
                used_elements.append(element.name)
            if element.rc:
                orientation = "-"
            else:
                orientation = "+"
            end = start + sizes[element.name] - 1
            print full_contig(group.name, start, end, counter, element.name, 1, sizes[element.name], orientation)
            start = end + 1
            counter += 1
            
            if element != group.elements[-1]:
                print unknown_gap(group.name, start, counter, size=1000)
                counter += 1
                start = start + 1000
                
    leftovers = [name for name in sizes if name not in used_elements]

    for name in leftovers:
        print full_contig("unanchored_" + name, 1, sizes[name], counter, name, 1, sizes[name], '+')
        counter += 1