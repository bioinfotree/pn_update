#!/usr/bin/perl
# takes in input a multifasta file with scaffold sequences
# and splits it into contigs and contextually produces AGP file
use warnings;
use strict;
use Getopt::Long;
use Pod::Usage;
use Switch;
use Bio::Perl;
use Bio::Seq;
use Bio::SeqIO;

my $scaffold; # input
my $minN = 60; # maximum length of a contiguous strech of Ns (above it split the sequence)
my $contigs; # output
my $agp; # output

my $help    = 0;

GetOptions(
        'scaffold=s' => \$scaffold,
        'minN=i' => \$minN,
        'contigs=s' => \$contigs,
        'agp=s' => \$agp,
        'help|?'       => \$help
) or pod2usage(2);
pod2usage(1) if $help;
pod2usage(1) if (!defined($scaffold) or !defined($contigs) or !defined($agp));

my $seqin = Bio::SeqIO->new(-format => 'Fasta',
			-file => "<".$scaffold);

my $seqout = Bio::SeqIO->new(-format => 'Fasta',
                          -file => '>'.$contigs);

open(AGP, ">".$agp) or die("Cannot open output agp file ".$agp."\n");
print AGP "##agp-version 2.0\n";

my $current_seq;

while ($current_seq = $seqin->next_seq()) {
#print $current_seq->display_id."\n";
	my $contig_counter = 1;
	my $part_number = 1;
	my $current_begin = 1;
	my $current_end;
	my $seq = $current_seq->seq();
	while ($seq =~ /(N{$minN}N*)/gc) {
		my $gap = $1;
		my $gap_length = length($gap);
		$current_end = pos($seq) - $gap_length;
		my $contig_length = $current_end - $current_begin + 1;
		my $contig_seq = substr($seq, $current_begin - 1, $contig_length);
		my $tempSeq = Bio::Seq->new(-seq => $contig_seq,
					    -type => 'Dna',
					    -ffmt => 'Fasta',
					    -desc => $current_seq->desc,
#					    -desc => $current_seq->desc . " [organism=Lactobacillus rossiae] [strain=DSM 15814]",
					    -id => $current_seq->display_id . "_" . $contig_counter);
		$seqout->write_seq($tempSeq);
		print AGP $current_seq->display_id."\t".$current_begin."\t".$current_end."\t".$part_number."\tW\t".$current_seq->display_id."_".$contig_counter."\t1\t".$contig_length."\t+\n";
		$part_number++;
		$current_begin = $current_end + 1;
		$current_end += $gap_length;
		$contig_counter++;
		print AGP $current_seq->display_id."\t".$current_begin."\t".$current_end."\t".$part_number."\tN\t".$gap_length."\tscaffold\tyes\tpaired-ends\n";
		$part_number++;
		$current_begin = $current_end + 1;
	}
	if ($contig_counter == 1) {
		my $tempSeq = Bio::Seq->new(-seq => $seq,
					    -type => 'Dna',
					    -ffmt => 'Fasta',
					    -desc => $current_seq->desc,
#					    -desc => $current_seq->desc . " [organism=Lactobacillus rossiae] [strain=DSM 15814]",
					    -id => $current_seq->display_id . "_" . $contig_counter);
		$seqout->write_seq($tempSeq);
		my $contig_length = length($seq);
#		$current_end = $current_begin + $contig_length - 1;
		$current_end = $contig_length;
		print AGP $current_seq->display_id."\t".$current_begin."\t".$current_end."\t".$part_number."\tW\t".$current_seq->display_id."_".$contig_counter."\t1\t".$contig_length."\t+\n";
	}
	else {
		my $contig_seq = substr($seq, pos $seq);
		my $tempSeq = Bio::Seq->new(-seq => $contig_seq,
					    -type => 'Dna',
					    -ffmt => 'Fasta',
					    -desc => $current_seq->desc,
#					    -desc => $current_seq->desc . " [organism=Lactobacillus rossiae] [strain=DSM 15814]",
					    -id => $current_seq->display_id . "_" . $contig_counter);
		$seqout->write_seq($tempSeq);
		my $contig_length = length($contig_seq);
		$current_end = $current_begin + $contig_length - 1;
		print AGP $current_seq->display_id."\t".$current_begin."\t".$current_end."\t".$part_number."\tW\t".$current_seq->display_id."_".$contig_counter."\t1\t".$contig_length."\t+\n";
	}
}


__END__


=head1 GFF Statistics

scaffold2contigs-AGP.pl - Using this script

=head1 SYNOPSIS

perl scaffold2contigs-AGP.pl [--scaffold <filename>] [--minN <integer>] [--contigs <filename>] [--agp <filename>] [--help]

  Options:
    --scaffold <filename>	input multifasta sequence to be split
    --minN <integer>		minimum length of a contiguous stretch of Ns to split a scaffold, default split at 60bp
    --contigs <filename>	output multifasta file with contigs
    --agp <filename>		output agp file
    --help                      print this help message
