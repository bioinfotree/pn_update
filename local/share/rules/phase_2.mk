# Copyright 2016 Michele Vidotto <michele.vidotto@gmail.com>

context prj/aln_genomes2

extern ../phase_1/filtered.assembly.bam_pseudomol.agp as THIS_FILTERED_ASSEMBLY_AGP
extern ../phase_1/reference.fasta.fai as THIS_REFERENCE_FASTA_FAI
extern ../phase_1/contigs.fasta as THIS_CONTIG_FASTA
extern ../phase_1/assembly.bed as THIS_ASSEMBLY_BED
extern ../phase_1/scaffolds.fasta as THIS_SCAFFOLD_FASTA

#### TEST CONVERSION

agp:
	ln -sf $(THIS_FILTERED_ASSEMBLY_AGP) $@

chain: agp
	cat $(THIS_REFERENCE_FASTA_FAI) \
	| agp2chain -f <(bawk '!/Unanchored/ { print $$0 }' <$<) >$@

reverse.chain: chain
	$(call load_modules); \
	chainSwap $< $@

reverse.agp: reverse.chain
	fasta_length <$(THIS_CONTIG_FASTA) \
	| agp2chain --reverse -f $< >$@

#### CONVERSION

reference.agp:
	tab2agp \
	--paths $(REFERENCE_GROUP) \
	--element-column 2 \
	--orientation-column 3 \
	--all-contigs-size <(zcat $(REFERENCE_SCAFFOLD) | fasta_length) \
	>$@


.META: other.assembly.unique.bed other.assembly.bed this.pseudomol.bed
	1	chrom	scaffold_110
	2	chromStart	3258
	3	chromEnd	298051
	4	feaure	scaffold_391
	5	score	3
	6	strand	-
	7	featureStart	167
	8	featureEnd	326279
	9	itemRgb	255,0,0
	10	featureCount	1
	11	featureSizes	294793
	12	fakeStart	0

# scapa alignments: other.assembly scaffolds -> pn scaffolds
other.assembly.bed:
	ln -sf $(OTHER_ASSEMBLY_BED) $@

# scapa alignments: pn scaffolds -> this.pseudomol
this.pseudomol.bed:
	ln -sf $(THIS_ASSEMBLY_BED) $@

# chain file mapping: this.pseudomol-> other.assembly scaffolds
this.pseudomol.chain: reference.agp
	zcat <$(REFERENCE) \
	| fasta_length \
	| agp2chain -f $< >$@

this.pseudomol.rew.chain: this.pseudomol.chain
	$(call load_modules); \
	chainSwap $< $@

# pn scaffold -> this.pseudomol translated into
# corresponding scaffolds
this.scaffold.bed: this.pseudomol.chain this.pseudomol.bed
	$(call load_modules); \
	CrossMap.py bed $< <(cut -f 1-8 <$^2) \
	| cut -f 10- \
	| select_columns 4 7 8 1 5 6 2 3 \
	| sortBed -i stdin >$@

.META: scaffold.vs.pseudomol2scaffold
	1	common scaffold and length
	2	unique to other.assembly.bed scaffold and length
	3	unique to this.pseudomol.bed scaffold and length
	4	unplaced scaffolds and length

# pn scaffolds common, unique, not used between:
#	other.assembly scaffolds -> pn scaffolds
#	pn scaffolds -> this.pseudomol
scaffold.vs.pseudomol2scaffold: other.assembly.bed this.pseudomol.bed $(THIS_SCAFFOLD_FASTA)
	paste \
	<( comm -12 --check-order <(select_columns 1 <$< | bsort | uniq) <(select_columns 4 <$^2 | bsort | uniq) | translate -a <(fasta_length <$^3) 1 | bsort -rn -k 2,2 | tr '\t' ' ') \   * common *
	<( comm -23 --check-order <(select_columns 1 <$< | bsort | uniq) <(select_columns 4 <$^2 | bsort | uniq) | translate -a <(fasta_length <$^3) 1 | bsort -rn -k 2,2 | tr '\t' ' ') \   * unique to FILE1 *
	<( comm -13 --check-order <(select_columns 1 <$< | bsort | uniq) <(select_columns 4 <$^2 | bsort | uniq) | translate -a <(fasta_length <$^3) 1 | bsort -rn -k 2,2 | tr '\t' ' ') \   * unique to FILE2 *
	<(fasta_length <$^3 | filter_1col -v 1 <(comm --check-order <(select_columns 1 <$< | bsort | uniq) <(select_columns 4 <$^2 | bsort | uniq) | sed -e 's/^[ \t]*//') | bsort -rn -k 2,2 | tr '\t' ' ') \   * unplaced *
	>$@

# pn scaffolds uniquely involved into other.assembly scaffolds -> pn scaffolds
other.assembly.unique.bed: scaffold.vs.pseudomol2scaffold other.assembly.bed
	filter_1col 1 \
	<(cut -f2 $< \ 
	| sed '/^\s*$$/d' \
	| tr ' ' '\t' \
	| cut -f1) <$^2 >$@

# reverse coordinates the bed file from:
#	pn scaffolds	other.assembly scaffolds
# 	to
#	other.assembly scaffolds	pn scaffolds
# NB:
# il formato BED riporta le coordinate di una regione relativa ad una specifica sequenza,
# magari riportata in un file.
# lo strand + indica che la sequenza di tale regione deve essere riportata così come appare dall'inizio alla fine
# lo strand - indica invece che la sequenza estratta dalla regione deve essere presentata rew-complementata.
# per quanto  rigarda il BED si scapa, uno strand - indica che:
# sequenza estratta da coordinate cromosomiche e rewcomp = sequenza estratta da coordinate di feature
# scambiando le colonne 1 2 3 con 4 7 8 accade il contrario:
# sequenza estratta da coordinate di feature rewcomp = sequenza estratta da coordinate cromosomiche
other.assembly.unique.rew.bed: other.assembly.unique.bed
	select_columns 4 7 8 1 5 6 2 3 <$< >$@

# the reverse complement of reference coordinates give the same sequence as normal coordinates of feaure
other.assembly.unique.fasta: other.assembly.unique.bed $(THIS_SCAFFOLD_FASTA)
	$(call load_modules); \
	bedtools getfasta -s -fi $^2 -bed $< -fo $@

other.assembly.unique.rew.fasta: other.assembly.unique.rew.bed ../../rkatsiteli4_pn_scaff/phase_1/scaffolds.fasta
	$(call load_modules); \
	bedtools getfasta -fi $^2 -bed $< -fo $@

# .PHONY: compare
# compare: other.assembly.unique.fasta other.assembly.unique.rew.fasta
	# diff -y <(fasta2tab <$<) <(fasta2tab <$^2) | less


# map other.assembly scaffolds to other.assembly this.pseudomol
# since I'm not sure how to change strand when reverse feature and chr, reverse the bed to the original form
# sort by pn sanger scaffolds then position, then by this.pseudomol
scaffold2pseudomol.bed: this.pseudomol.rew.chain other.assembly.unique.rew.bed
	$(call load_modules); \
	CrossMap.py bed $< $^2 \
	| cut -f 10- \
	| select_columns 4 7 8 1 5 6 2 3 \   * reverse bed to original shape *
	| sed '/^\s*$$/d' \
	| bsort -k 1,1 -k 2,2n -k 7,7 >$@



scaffold2pseudomol.bed.fasta: scaffold2pseudomol.bed
	$(call load_modules); \
	bedtools getfasta -fi reference.fasta -bed $< -fo $@

scaffold2pseudomol.renamed.bed: scaffold2pseudomol.bed
	bawk '!/^[\#+,$$]/ { \
	if ( $$4 ~ /^scaffold/ ) { \
		sub(/scaffold/, "group", $$4); \
		print $$0; } \
	else { \
		print $$0; } \
	}' $< >$@



# .PHONY: compare2
# compare2: scaffold2pseudomol.bed.fasta other.assembly.unique.fasta
	# diff -y <(fasta2tab <$^2) <(fasta2tab <$<) >$@

# get statistics of this.pseudomol and corresponding pn scaffolds
scaffold2pseudomol.stat: scaffold2pseudomol.bed
	paste \
	<(bawk '$$4 ~ /^group/' $< | cut -f4 - | bsort | uniq | wc -l) \
	<(bawk '$$4 ~ /^group/' $< | cut -f1 - | bsort | uniq | wc -l) \
	<(bawk '$$4 ~ /^unanchored/' $< | cut -f4 - | bsort | uniq | wc -l) \
	<(bawk '$$4 ~ /^unanchored/' $< | cut -f1 - | bsort | uniq | wc -l) \
	| sed '1s/^/pseudomol\tscaffold in psedomol\tunancored\tscaffold in unanchored\n/' >$@



.PHONY: test
test:
	@echo 


ALL += scaffold2pseudomol.renamed.bed \
	other.assembly.unique.bed

INTERMEDIATE += 

CLEAN += 
