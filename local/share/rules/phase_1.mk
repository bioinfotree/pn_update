# Copyright 2014 Michele Vidotto <michele.vidotto@gmail.com>

# heterozygosity Vs coverage of contigs on the reference

# TODO:

context prj/aln_genomes2

# reference
REFERENCE ?=

# contigs
CONTIGS ?=

# scaffolds
SCAFFOLDS ?=

MIN_REF_LEN ?= 50000

# log dir
log:
	mkdir -p $@


contigs.fasta:
	zcat <$(CONTIGS) >$@

scaffolds.fasta:
	zcat <$(SCAFFOLDS) >$@


final.summary:
	ln -sf $(FINAL_SUMMARY) $@

# denom can't handle to short references
reference.fasta:
	zcat $(REFERENCE) \
	| fasta2tab \
	| tr " " \\t \
	| awk 'BEGIN { OFS = "\t" } \
	!/^[\#+,$$]/ { \
	if ( length($$NF) >= $(MIN_REF_LEN) ) print $$1,$$NF; }' \   * filter is needed by denom *
	| bsort \
	| tab2fasta 2 >$@


# save list of chunks to make var
chunks.mk:
	!threads
	ARRAY=( $$(seq -s " " 8) ); \
	echo "FASTA_CHUNCKS := $${ARRAY[@]/#/contigs.fasta.}" >$@

include chunks.mk


# split fasta
contigs.fasta.%: contigs.fasta
	$(call load_modules); \
	if [ "$*" == "1" ]; then \ 
		gt splitfasta -numfiles 8 -force yes $<; \
	else \
		sleep 5; \   * wait untill all pieces are generated *
	fi



# execute denom for every chunks
# it runs in succession the indexing and alignment. 
# In oreder to avoid to rebuild indexs for each chunk, I wait for it to finish indexing and allignment of chuck 1.
# For chuncks >1, I create links for indexs in directory 1 to their respective directories. I run denom easyrun.
# It finds the index files and proceeds whith the alignment, that can be executed in parallel for all the chunk> 1.
FASTA_BAMS = $(addsuffix .bam,$(addprefix assembly.,$(shell seq 8)))
assembly.%.bam: reference.fasta contigs.fasta.% log
	$(call load_modules); \
	mkdir -p $*; \
	if [ "$*" == "1" ]; then \
		cd $*; \
		ln -sf ../$< .; \
		ln -sf ../$^2 .; \
		/usr/bin/time -v denom easyrun $< $^2 $@ $(basename $@).sdi 2>../$^3/denom-easyrun.$@.log \
		&& cd ..; \
		ln -sf $*/$@ .; \
	else \
		until [ -s 1/assembly.1.bam ]; do \   * wait for assembly.1.bam to be ready *
			sleep 15; \
			echo "process for chunk $* is spleeping 15 seconds.."; \
		done; \
		cd $*; \
		ln -sf ../$< .; \
		ln -sf ../$^2 .; \
		ln -sf ../1/$<.amb .; \
		ln -sf ../1/$<.ann .; \
		ln -sf ../1/$<.bwt .; \
		ln -sf ../1/$<.pac .; \
		ln -sf ../1/$<.fai .; \
		ln -sf ../1/$<.sa .; \
		/usr/bin/time -v denom easyrun $< $^2 $@ $(basename $@).sdi 2>../$^3/denom-easyrun.$@.log \
		&& cd ..; \
		ln -sf $*/$@ .; \
	fi



# merge and sort the bams
assembly.bam: $(FASTA_BAMS)
	!threads
	$(call load_modules); \
	samtools merge -f \
	- $^ \
	| samtools sort -@ $$THREADNUM - $(basename $@)



# generate bam index
assembly.bam.bai: assembly.bam
	$(call load_modules); \
	samtools index $<


.META: assembly.sdi
	1	chromosome	The same name as the chromosome id in reference fasta file
	2	position	1-based leftmost position
	3	length	the length difference of the changed sequence against reference (0 for SNPs, negative for deletions, positive for insertions)
	4	reference base [-A-Z]+	(regular expression range)
	5	consensus base [-A-Z]+	((regular expression range), IUPAC code is used for heterozygous sites
	6	quality value	* means no value available; [0-9]+ shows the quality of this variant. It is not necessary Phred quality.
	7	percentage value	* means no value available; 0~100 shows for whether the variant is heterozygous (used for INDELs) 

# denovo call variants using denom varcall
# after margin I have to do it again
assembly.sdi: reference.fasta assembly.bam
	$(call load_modules); \
	denom varcall --outputfile $@ $< $^2


### ScaPA ################################################################

.META: filtered.assembly.bam_placed-full.txt
	1	query_scaff
	2	query_start
	3	query_end
	4	query_span
	5	scaffold_length
	6	subj_chr
	7	chr_start
	8	chr_end
	9	chr_span
	10	query_coverage
	11	parsimony
	12	orientation
	13	SW-anchored
	14	SCORE
	15	identity
	16	glob_span
	17	seq_span
	18	gaps_cumul
	19	self.aligned
	20	alignments
	21	merged_object


# export alignments of the golden block for each placed scaffold
# in bed-12 format

# $(call load_modules,1,2,3,4)
# 1: alignment-files
# 2: sspace-evidence
# 3: contig-fasta
# 4: outfile
filtered.assembly.bam_placed-full.txt: assembly.bam final.summary contigs.fasta
	module purge; \
	module load lang/python/2.7.3; \   * pysam-0.8.2.1 and biopython 1.60 needed *
	ScaPA_alpha_r004 \
	$(call scapa_param,$<,$^2,$^3,$@)

filtered.assembly.bam_ambiguous.txt: filtered.assembly.bam_placed-full.txt
	touch $@

filtered.assembly.bam_placed.alignments.bed: filtered.assembly.bam_placed-full.txt
	touch $@

filtered.assembly.bam_placed-splits.txt: filtered.assembly.bam_placed-full.txt
	touch $@

filtered.assembly.bam_unaligned: filtered.assembly.bam_placed-full.txt
	touch $@



# tranform alignments to bed12 format
assembly.bamToBed.bed: assembly.bam
	$(call load_modules); \
	bamToBed \
	-color "255,0,0" \
	-cigar \
	-split \
	-bed12 \
	-i $< \
	>$@

# contains placed scaffolds. 
# The coordinates corresponds to the blocks that anchor scaffolds to the reference
assembly.placed-full.bed: filtered.assembly.bam_placed-full.txt
	$(call load_modules); \
	unhead $< \
	| bawk 'BEGIN { strand=""; } \
	!/^[\#,$$]/ { \
	if ( $$12 > 0 ) { strand="+"; } else { strand="-"; } \
	feature_start=$$2; feature_end=$$3; \
	if ( $$2 > $$3 ) { feature_start=$$3; feature_end=$$2 } \   * reverse feature coordinates if start > end *
	print $$6,$$7,$$8,$$1,int(100^$$14),strand,feature_start,feature_end,"255,0,0","1",$$9,"0"; \
	}' \
	| sortBed -i stdin >$@

# contains placed scaffolds as assembly.bed
# In addition to the scaffolds placed in a clean way, 
# it also contains the scaffolds placed after the split operation.
assembly.swap.bed: filtered.assembly.bam_placed-full.txt filtered.assembly.bam_placed-splits.txt filtered.assembly.bam_ambiguous.txt
	$(call load_modules); \
	cat <(unhead $<) <(unhead $^2) \
	| bawk 'BEGIN { strand=""; } \
	!/^[\#,$$]/ { \
	if ( $$12 > 0 ) { strand="+"; } else { strand="-"; } \
	feature_start=$$2; feature_end=$$3; \
	if ( $$2 > $$3 ) { feature_start=$$3; feature_end=$$2 } \   * reverse feature coordinates if start > end *
	print $$1,feature_start,feature_end,$$6,$$7,$$8,int(100^$$14),strand; \
	}' \
	| sortBed -i stdin >$@


# # assembly.aligns.bed contains contigs.
# # The coordinates corresponds to the alignments that anchor contigs to the reference
# assembly.aligns.bed: filtered.assembly.bam_aligns.bed
# 	$(call load_modules); \
# 	sortBed -i $< \
# 	| mergeBed -c 4,5,13 \
# 	-delim ";" \
# 	-o distinct,distinct,distinct \
# 	-i stdin >$@

# contains contigs.
# The coordinates corresponds to the alignments that anchor contigs to the reference
# only for placed scaffolds (not splitted)
# ScaPA_alpha_r002 now produce bed file without conversion
assembly.aligns.bed: filtered.assembly.bam_placed.alignments.bed
	$(call load_modules); \
	sortBed -i $< >$@

### ScaPA ################################################################

length.unaligned.txt: filtered.assembly.bam_unaligned.txt scaffolds.fasta
	translate <(fasta_length $^2) 1 <$< \
	| bsort -nr \
	| sed '1s/^/scaffold_length\n/' >$@

# custom script
lenght.histogram.png: filtered.assembly.bam_placed-full.txt filtered.assembly.bam_placed-splits.txt filtered.assembly.bam_ambiguous.txt length.unaligned.txt
	print_histogram $< $^2 $^3 $^4 $@


.PHONY: test
test:
	@echo 


ALL += contigs.fasta \
	assembly.bam \
	assembly.bam.bai \
	assembly.sdi \
	\
	assembly.bed \
	assembly.placed-full.bed \
	assembly.aligns.bed


INTERMEDIATE += reference.fasta \
		$(FASTA_BAMS) \
		$(FASTA_CHUNCKS)

CLEAN += chunks.mk \
	 $(wildcard reference.*) \
	 $(shell seq 8)
