# Copyright 2014 Michele Vidotto <michele.vidotto@gmail.com>

# heterozygosity Vs coverage of contigs on the reference

# TODO:

context prj/aln_genomes2

# reference
REFERENCE ?=

# contigs
CONTIGS ?=

# scaffolds
SCAFFOLDS ?=

MIN_REF_LEN ?= 50000

# log dir
log:
	mkdir -p $@



# remove extra description in fasta header
# moreover in order to generate correct SSPACE evidence file
# from AGP, contigs in the fasta file must have the same
# order as they appear in the AGP
contigs.fasta:
	bawk '!/^[\#+,$$]/ { if ( $$5 == "W" ) print $$6 }' <$(AGP) \
	| translate -a <(zcat <$(CONTIGS) \
	| fasta2tab \
	| tr " " \\t \
	| awk 'BEGIN { OFS = "\t" } !/^[\#+,$$]/ { print $$1,$$NF; }') \
	1 \
	| tab2fasta -s 2 >$@

scaffolds.fasta:
	zcat <$(SCAFFOLDS) \
	| fasta2tab \
	| tr " " \\t \
	| awk 'BEGIN { OFS = "\t" } !/^[\#+,$$]/ { print $$1,$$NF; }'\
	| bsort \
	| tab2fasta 2 >$@



# Transform an AGP in the following form:
# ##agp-version 2.0
# scaffold_0	1	6706	1	W	scaffold_0_1	1	6706	+
# scaffold_0	6707	6806	2	N	100	scaffold	yes	paired-ends
# scaffold_0	6807	17361	3	W	scaffold_0_2	1	10555	+
# scaffold_0	17362	18348	4	N	987	scaffold	yes	paired-ends

# to a .scaffolds.evidence file of SSPACE:

# >scaffold_0|size13101952|tigs204
# f_tig1|size6706|links12|gaps100
# f_tig2|size10555|links12|gaps987
# f_tig3|size68901|links12|gaps100
# f_tig4|size24070|links12|gaps100
# f_tig5|size11554|links12|gaps2615

# The first line indicates the scaffold, which is the same as in the .scaffolds.fasta file. 
# Next, for each contig the connection (orientation, links and gaps) with other contigs are given.
# The second line for example means forward contig 5 with size 728 has 12 links and a gap of 100bp
# with reverse contig 1. If a line ends with <merged>, it means that the contig has overlap with
# the next contig, and they are merged. For contig f_tig100, 40 nucleotides had an overlap
# with contig f_tig91.

# AGP
# contig
# scaffold_0	1	6706	1	W	scaffold_0_1	1	6706	+
# gap
# scaffold_0	6707	6806	2	N	100	scaffold	yes	paired-ends
scaffolds.evidence: scaffolds.fasta
	unhead <$(AGP) | cut -f1 | uniq \   * get the order of the scaffolds in the AGP *
	| translate -a <(unhead <$(AGP) \   * collapsed returns scaffolds in different order. Here the scaffolds are re-ordered as in the AGP *
	| sed -e 's/\t/:/' -e 's/\t/ /g' -e 's/:/\t/' \   * transform all but the first tab into sapce *
	| collapsesets -o 2) \   * all rows of a same scaffold are collapsed in unique row *
	1 \
	| translate -a <(fasta_length <$<) 1 \   * append scaffold lengths *
	| bawk 'BEGIN { \
	scaffold=""; \
	scaffold_len=0; \
	contig=""; \
	contig_start=0; \
	contig_end=0; \
	contig_span=0; \
	gap_span=0; \
	contig_num=0; \
	contig_position=0; \
	strand=""; \
	} !/^[\#+,$$]/ { \
	\
	split($$3,line,";"); \
	scaffold=$$1; \
	scaffold_len=$$2; \
	for ( i=0; i<length(line); i++ ) { \   * calculate number of contigs per scaffold *
		split(line[i],item," "); \
		component=item[4]; \
		if ( component == "W" ) { contig_num++ }; \
	} \
	printf "\>%s|size%i|tigs%i\n", scaffold, scaffold_len, contig_num; \   * print header row of the scaffold *
	\
	for ( i=0; i<length(line); i++ ) { \
		split(line[i],item," "); \
		component=item[4]; \
		if ( component == "W" ) { \   * calculate contig features *
			contig=item[5]; \
			contig_position++; \
			contig_start=item[6]; \
			contig_end=item[7]; \
			contig_span=contig_end - contig_start + 1; \
			if ( item[8] == "+" ) { strand="f" } else { strand="r" }; \
			printf "%s_tig%i|size%i", strand, contig_position, contig_span; \   * print contig features *
		} \
		if ( component == "N" ) { \   * calculate gap features *
			gap_span=item[5]; \
			printf "|links%i|gaps%i\n", 12, gap_span; \   * print gap features *
		} \
	} \
	print "\n"; \   * newline of at the end of the scaffold lines *
	}' >$@


reference.fasta:
	zcat <$(REFERENCE) \
	| fasta2tab \
	| tr " " \\t \
	| awk 'BEGIN { OFS = "\t" } !/^[\#+,$$]/ { print $$1,$$NF; }'\
	| bsort \
	| tab2fasta 2 >$@


# save list of chunks to make var
chunks.mk:
	!threads
	ARRAY=( $$(seq -s " " 8) ); \
	echo "FASTA_CHUNCKS := $${ARRAY[@]/#/contigs.fasta.}" >$@

include chunks.mk


# split fasta
contigs.fasta.%: contigs.fasta
	$(call load_modules); \
	if [ "$*" == "1" ]; then \ 
		gt splitfasta -numfiles 8 -force yes $<; \
	else \
		sleep 5; \   * wait untill all pieces are generated *
	fi



# execute denom for every chunks
# it runs in succession the indexing and alignment. 
# In oreder to avoid to rebuild indexs for each chunk, I wait for it to finish indexing and allignment of chuck 1.
# For chuncks >1, I create links for indexs in directory 1 to their respective directories. I run denom easyrun.
# It finds the index files and proceeds whith the alignment, that can be executed in parallel for all the chunk> 1.
FASTA_BAMS = $(addsuffix .bam,$(addprefix assembly.,$(shell seq 8)))
assembly.%.bam: reference.fasta contigs.fasta.% log
	$(call load_modules); \
	mkdir -p $*; \
	if [ "$*" == "1" ]; then \
		cd $*; \
		ln -sf ../$< .; \
		ln -sf ../$^2 .; \
		/usr/bin/time -v denom easyrun $< $^2 $@ $(basename $@).sdi 2>../$^3/denom-easyrun.$@.log \
		&& cd ..; \
		ln -sf $*/$@ .; \
	else \
		until [ -s 1/assembly.1.bam ]; do \   * wait for assembly.1.bam to be ready *
			sleep 15; \
			echo "process for chunk $* is spleeping 15 seconds.."; \
		done; \
		cd $*; \
		ln -sf ../$< .; \
		ln -sf ../$^2 .; \
		ln -sf ../1/$<.amb .; \
		ln -sf ../1/$<.ann .; \
		ln -sf ../1/$<.bwt .; \
		ln -sf ../1/$<.pac .; \
		ln -sf ../1/$<.fai .; \
		ln -sf ../1/$<.sa .; \
		/usr/bin/time -v denom easyrun $< $^2 $@ $(basename $@).sdi 2>../$^3/denom-easyrun.$@.log \
		&& cd ..; \
		ln -sf $*/$@ .; \
	fi



# merge and sort the bams
assembly.bam: $(FASTA_BAMS)
	!threads
	$(call load_modules); \
	samtools merge -f \
	- $^ \
	| samtools sort -@ $$THREADNUM - $(basename $@)



# generate bam index
assembly.bam.bai: assembly.bam
	$(call load_modules); \
	samtools index $<


.META: assembly.sdi
	1	chromosome	The same name as the chromosome id in reference fasta file
	2	position	1-based leftmost position
	3	length	the length difference of the changed sequence against reference (0 for SNPs, negative for deletions, positive for insertions)
	4	reference base [-A-Z]+	(regular expression range)
	5	consensus base [-A-Z]+	((regular expression range), IUPAC code is used for heterozygous sites
	6	quality value	* means no value available; [0-9]+ shows the quality of this variant. It is not necessary Phred quality.
	7	percentage value	* means no value available; 0~100 shows for whether the variant is heterozygous (used for INDELs) 

# denovo call variants using denom varcall
# after margin I have to do it again
assembly.sdi: reference.fasta assembly.bam
	$(call load_modules); \
	denom varcall --outputfile $@ $< $^2


### ScaPA ################################################################

.META: filtered.assembly.bam_placed-full.txt
	1	query_scaff
	2	query_start
	3	query_end
	4	query_span
	5	scaffold_length
	6	subj_chr
	7	chr_start
	8	chr_end
	9	chr_span
	10	query_coverage
	11	parsimony
	12	orientation
	13	SW-anchored
	14	SCORE
	15	identity
	16	glob_span
	17	seq_span
	18	gaps_cumul
	19	self.aligned
	20	alignments
	21	merged_object


# export alignments of the golden block for each placed scaffold
# in bed-12 format

# $(call load_modules,1,2,3,4)
# 1: alignment-files
# 2: sspace-evidence
# 3: contig-fasta
# 4: outfile
filtered.assembly.bam_placed-full.txt: assembly.bam scaffolds.evidence contigs.fasta
	module purge; \
	module load lang/python/2.7.3; \   * pysam-0.8.2.1 and biopython 1.60 needed *
	ScaPA_alpha_r004 \
	$(call scapa_param,$<,$^2,$^3,$@)


filtered.assembly.bam_placed.alignments.bed: filtered.assembly.bam_placed-full.txt
	touch $@

filtered.assembly.bam_placed-splits.txt: filtered.assembly.bam_placed-full.txt
	touch $@

filtered.assembly.bam_unaligned: filtered.assembly.bam_placed-full.txt
	touch $@

# tranform alignments to bed12 format
assembly.bamToBed.bed: assembly.bam
	$(call load_modules); \
	bamToBed \
	-color "255,0,0" \
	-cigar \
	-split \
	-bed12 \
	-i $< \
	>$@

# contains placed scaffolds. 
# The coordinates corresponds to the blocks that anchor scaffolds to the reference
assembly.placed-full.bed: filtered.assembly.bam_placed-full.txt
	$(call load_modules); \
	unhead $< \
	| bawk 'BEGIN { strand=""; } \
	!/^[\#,$$]/ { \
	if ( $$12 > 0 ) { strand="+"; } else { strand="-"; } \
	feature_start=$$2; feature_end=$$3; \
	if ( $$2 > $$3 ) { feature_start=$$3; feature_end=$$2 } \   * reverse feature coordinates if start > end *
	print $$6,$$7,$$8,$$1,int(100^$$14),strand,feature_start,feature_end,"255,0,0","1",$$9,"0"; \
	}' \
	| sortBed -i stdin >$@


.META: assembly.bed
	1	chrom	scaffold_110
	2	chromStart	3258
	3	chromEnd	298051
	4	feaure	scaffold_391
	5	score	3
	6	strand	-
	7	featureStart	167
	8	featureEnd	326279
	9	itemRgb	255,0,0
	10	featureCount	1
	11	featureSizes	294793
	12	fakeStart	0

# contains placed scaffolds as assembly.bed
# In addition to the scaffolds placed in a clean way, 
# it also contains the scaffolds placed after the split operation.
assembly.bed: filtered.assembly.bam_placed-full.txt filtered.assembly.bam_placed-splits.txt filtered.assembly.bam_ambiguous.txt
	$(call load_modules); \
	cat <(unhead $<) <(unhead $^2) \
	| bawk 'BEGIN { strand=""; } \
	!/^[\#,$$]/ { \
	if ( $$12 > 0 ) { strand="+"; } else { strand="-"; } \
	feature_start=$$2; feature_end=$$3; \
	if ( $$2 > $$3 ) { feature_start=$$3; feature_end=$$2 } \   * reverse feature coordinates if start > end *
	print $$6,$$7,$$8,$$1,int(100^$$14),strand,feature_start,feature_end,"255,0,0","1",$$9,"0"; \
	}' \
	| sortBed -i stdin >$@

assembly.rew.bed: assembly.bed
	$(call load_modules); \
	select_columns 4 7 8 1 5 6 2 3 <$< \
	| sortBed -i stdin >$@

# # assembly.aligns.bed contains contigs.
# # The coordinates corresponds to the alignments that anchor contigs to the reference
# assembly.aligns.bed: filtered.assembly.bam_aligns.bed
# 	$(call load_modules); \
# 	sortBed -i $< \
# 	| mergeBed -c 4,5,13 \
# 	-delim ";" \
# 	-o distinct,distinct,distinct \
# 	-i stdin >$@

# contains contigs.
# The coordinates corresponds to the alignments that anchor contigs to the reference
# only for placed scaffolds (not splitted)
# ScaPA_alpha_r002 now produce bed file without conversion
assembly.aligns.bed: filtered.assembly.bam_placed.alignments.bed
	$(call load_modules); \
	sortBed -i $< >$@

### ScaPA ################################################################









.PHONY: test
test:
	@echo 


ALL += contigs.fasta \
	assembly.bam \
	assembly.bam.bai \
	assembly.sdi \
	\
	assembly.bed \
	assembly.rew.bed \
	assembly.placed-full.bed \
	assembly.aligns.bed


INTERMEDIATE += reference.fasta \
		$(FASTA_BAMS) \
		$(FASTA_CHUNCKS)

CLEAN += chunks.mk \
	 $(wildcard reference.*) \
	 $(shell seq 8)
